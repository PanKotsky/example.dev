<?php

namespace Controller;

use Model\TypeArea;
use \Smarty\View;
use \Model\Item;
use \Model\User;
use \Model\ItemCores;
use \Model\ULogin;
use Model\GalleryImages;

use \Request;


class Index extends \Builder
{
    /**
     * @var \smarty\View
     */
    public $template;
    public $item_limit_navigation = 30;
    public $item_rating = 2;
    private $item_id = 9;

    public function action_index()
    {

        \Assets::css('page1', base_UI.'css/style_1200px.css');
        \Assets::css('page2', base_UI.'css/style_1199px_586px.css');
        \Assets::js('page1', base_UI.'js/pages/hover.js');
        \Assets::js('page3', 'https://apis.google.com/js/platform.js');//, 'async defer');

        //<meta name="google-site-verification" content="TWdR__5Bbsu_02-ttl2WQkM7go6Vo1VofON5JMlata8" />

        \Assets::js('rating', base_UI.'js/plugins/jRating-master/jquery/jRating.jquery.js');
        \Assets::css('rating',base_UI.'js/plugins/jRating-master/jquery/jRating.jquery.css');

        $this->returnUrl();

        $user = \Model\User::model()->findByPk( $this->user_id  );

        $data_world_regions   = $this->dataWorldRegions();
//        $data_area_types      = $this->dataAreaTypes();

        /* Під страхом владикі Ктулху забороняю видаляти один з цих запитів */

        $criteria = new \DBCriteria();
        $criteria->select = 'type_area.id, count(it.id) AS count_items, type_area.name';
        $criteria->group = 'type_area.id';
        $criteria->index = 'id';
//        $criteria->order = 'type_area.name ASC';
        $criteria->mergeWith(array(
            'join' => 'LEFT JOIN item_type ON item_type.type_id = type_area.id
                        LEFT JOIN item AS it ON it.id = item_type.item_id'
        ));

        $data_area_types = TypeArea::model()->findAll($criteria);

        $criteria = new \DBCriteria();
        $criteria->select = 'type_area.id, count(it.id) AS count_items';
        $criteria->condition = ' it.validate = 1';
        $criteria->group = 'type_area.id';
        $criteria->index = 'id';
        $criteria->mergeWith(array(
            'join' => 'LEFT JOIN item_type ON item_type.type_id = type_area.id
                        LEFT JOIN item AS it ON it.id = item_type.item_id'
        ));

        $data_area_types_count = TypeArea::model()->findAll($criteria);

        foreach ($data_area_types as $key => $area_types) {
            if (isset($data_area_types_count[$key])) {
                $data_area_types[$key]->count_items = $data_area_types_count[$key]->count_items;
            }
            else {
                $data_area_types[$key]->count_items = 0;
            }
        }


        $data_sights          = $this->dataSights();
        $data_count          = $this->dataSightsCount();
        $data_images          = $this->getItemsImages( $data_sights );


        $this->template->assign([
            'data_world_regions'   => $data_world_regions,
            'data_area_types'  => $data_area_types,
            'data_sights'      => $data_sights,
            'data_count'       => $data_count,
            'data_images'      => $data_images,
            'user'             => $user
        ]);

        $this->response->body($this->template->fetch('main.tpl'));

    }

    public static function fbParams(){
        $FB_params = array(
            'client_id'     => 'тут клієнт_ід, але вам його не покажу',
            'redirect_uri'  => 'http://'.$_SERVER['HTTP_HOST'].'/Index/uloginAuth',
            'client_secret' => 'і це вам теж не покажу',
            'code'          => $_GET['code'],
            'scope'        => 'email,user_friends'
        );

        return $FB_params;
    }

    /**
     * gets info from social network. If profile already linked to user authenticates, otherwise create new user instance
     * @throws \Kohana_Database_Exception
     */
    public function action_uloginAuth()
    {
        \Assets::css('change', base_UI.'css/style.css');
        \Assets::js('BootBox', base_UI.'libs/BootBox/bootbox.js');

        $memberPreviousPage = $_SESSION['href'];
        unset($_SESSION['href']);

        $userInfo = null;

        if(isset($_GET['code'])) { //facebook

            $params = self::fbParams();

            $url = 'https://graph.facebook.com/oauth/access_token';
            $tokenInfo = null;

            /* Facebook раніше повертав дані звичайним рядком, відповідно використовувалась функція
             * parse_str , нині повертає json , тому довелось переписати, але про всяк випадок залишив
             * закоментованим попередній варіант парсинга
             */
//       parse_str(file_get_contents($url . '?' . http_build_query($params)), $tokenInfo);

            $str_data = file_get_contents($url . '?' . http_build_query($params));
            $tokenInfo = json_decode($str_data, true);


            if (count($tokenInfo) > 0 && isset($tokenInfo['access_token'])) {
                $params = array(
                    'access_token' => $tokenInfo['access_token'],
                    'fields' => 'id,email,first_name,last_name,gender,link',
                );
                //echo 'https://graph.facebook.com/me' . '?' . urldecode(http_build_query($params));
                $userInfo = json_decode(file_get_contents('https://graph.facebook.com/v2.6/me' . '?' . urldecode(http_build_query($params))), true);
                $userInfo['photo'] = 'http://graph.facebook.com/'.$userInfo['id'].'/picture?type=large';
                $userInfo['network'] = 'facebook';

            }
        }elseif( isset($_GET['gp']) ){ //google+
            $userInfo['id'] = isset( $_GET['id'] ) ? $_GET['id'] : '';
            $userInfo['first_name'] = isset( $_GET['firstname'] ) ? $_GET['firstname'] : '';
            $userInfo['last_name'] = isset( $_GET['lastname'] ) ? $_GET['lastname'] : '';
            $userInfo['email'] = isset( $_GET['email'] ) ? $_GET['email'] : '';
            $userInfo['photo'] = isset( $_GET['photo'] ) ? $_GET['photo'] : '';
            $userInfo['link'] = "https://plus.google.com/u/0/$userInfo[id]";
            $userInfo['network'] = 'google';

        }else{
            $this->redirect('/');
        }


        if(strlen($userInfo['error'])> 0)
        {
            $this->response->body($this->template->fetch('internal.tpl'));
            return;
        }

        $condition = (new \DBCriteria())
            ->addColumnCondition(
                [
                    'uid'    => $userInfo['id'],
                    'network' => $userInfo['network']
                ]);

        /** @var $ULogin \Model\ULogin */
        $ULogin = \Model\ULogin::model()->with('user')->find($condition);

        if(null === $ULogin)
        {
            \Session::instance()->set('UloginData',$userInfo);

            /** @var $user_model \Model\User */


            /* If this nickname is not available - please change your nickname */

            if($res = \Model\User::model()->findByAttributes(['nickname'=>$userInfo['first_name']])){

                $this->template->assign([
                    'nickname' => $userInfo['first_name']
                ]);
                $this->response->body($this->template->fetch('layout/confirm_login.tpl'));

                return;
            }


            /* else - include method with saving User*/

            elseif(!$res = \Model\User::model()->findByAttributes(['nickname'=>$userInfo['first_name']])){

                self::saveUserFromSocNetwork($userInfo, $this);
            }
        }
        else
        {
            $user_login   = \Model\User::model()->findByPk($ULogin->user_id);
            if($user_login->ban){
                $this->template->assign([
                    'message' => 'Your account has been deleted. If you want to sign up again please let us know at our Contact page'
                ]);

                $this->response->body($this->template->fetch('layout/banned_account.tpl'));
                return;
            }
            $user_login->last_login = time();
            $user_login->update();

            \Auth\Base::startSession($ULogin['user']);

            $memberPreviousPage ? $this->request->redirect($memberPreviousPage) : $this->request->redirect(\Route::get('pages')->uri(['controller'=>'index','action'=>'index']));

        }
    }


    /* This method save User in DB after changed nickname */

    public function action_SaveUserWithChangedNickname(){
        header("Content-type:text/html;charset=utf-8");  // for encoding page

        $userInfo = $_SESSION['UloginData'];
        $userInfo['nickname'] = $_POST['nickname'];

        /* include method with saving User */
        self::saveUserFromSocNetwork($userInfo, $this);
    }


    /* This method checked changed user nickname into form */

    public function action_checkNickname(){
        if( !empty($_POST['nickname'])){
            $search_nickname = $_POST['nickname'];
            if(!\Model\User::model()->findByAttributes(['nickname'=>$search_nickname])){
                echo "<img src=".\Kohana::$base_url."template/default/img/circle.png \" style=\"margin-bottom: 2px;\">";
            }
            elseif(\Model\User::model()->findByAttributes(['nickname'=>$search_nickname])){
                echo "<img src=".\Kohana::$base_url."template/default/img/cancel.png \" style=\"margin-bottom: 2px;\">";

            }
        }
        return;
    }

    /* This method is used to store user who signed up via social network
    *   Connects two action uLoginAuth and SaveUserWithChangedNickname
    */


    public static function saveUserFromSocNetwork($userInfo, $indexController)
    {

        /* if took place change nickname */
        $user_model = new User();
        if (isset($userInfo['nickname'])) {
            $user_model->nickname = $userInfo['nickname'];
        }

        /* if nickname is firstname in social network */
        else {
            $user_model->nickname = $userInfo['first_name'];
        }
        $user_model->fio      = $userInfo['first_name'].' '.$userInfo['last_name'];
        $user_model->email    = $userInfo['email'];
        $user_model->date_register = date('Y-m-d');


        $access_level = new \Auth\Access();
        $access_level->set(\Auth\Access::User_Login);
        $user_model->access_level =  $access_level->getValue();


        if(!$user_model->save())
        {
            throw new \Kohana_Database_Exception('Unable to save user model');
        }

        //Saving avatar
        if( $userInfo['photo'] != 'undefined' && $userInfo['photo'] != NULL ){
            $file = file_get_contents($userInfo['photo']);
            $fileName = str_shuffle ( 'abcdefghijklmn' ) . '.jpg';
            $fileDir = "Uploads/Photo/$user_model->id";

            if (!mkdir($fileDir, 0777, true)) {
                throw new \Kohana_Database_Exception('Unable to create directory');
            }
            echo 'console.log(q='.file_put_contents($fileDir.'/'.$fileName, $file) . ');';
            $user_model->photo = $fileName;
            if(!$user_model->save())
            {
                throw new \Kohana_Database_Exception('Unable to save avatar');
            }
        }

        $ULogin          = new ULogin();
        $ULogin->network = $userInfo['network'];
        $ULogin->uid     = $userInfo['id'];
        $ULogin->profile = $userInfo['link'];
        $ULogin->user_id = $user_model->id;

        if(!$ULogin->save())
        {
            $indexController->response->body('Unable to save social network data');

        }

        $user_login   = \Model\User::model()->findByPk($ULogin->user_id);
        $user_login->last_login = time();
        $user_login->update();

        \Auth\Base::startSession($ULogin['user']);
        $indexController->redirect(\Route::get('pages')->uri(['controller'=>'index','action'=>'index']));
    }


    public function dataWorldRegions()
    {
        $data = \Model\WorldRegion::model()->findAll();
        return $data;
    }

    public function dataAreaTypes()
    {
        $criteria = (new \DBCriteria());
        $criteria->select = 'itp.*, tp.id, tp.name, count( it.id ) as count_sights';

        $criteria->mergeWith(array(
            'join'=>'INNER JOIN item_type AS itp ON itp.item_id = it.id
                    INNER JOIN type_area AS tp ON tp.id = itp.type_id',
        ));

        $criteria->group = 'tp.id';
        $criteria->order = 'tp.name ASC';

        $data = \Model\Item::model()->findAll( $criteria );
        return $data;
    }

    public function dataSights()
    {
        $criteria    = (new \DBCriteria());
        $criteria->select = 'it.id, it.name, it.aid, it.center_lat, it.center_lng,
            ct.code, ct.name as cname, sum(value)/count(value) as rating, count(value) as rating_count';

        $criteria->mergeWith(array(
            'join'=>'INNER JOIN item_has_locations AS ug ON ug.id = it.id
                     INNER JOIN countries AS ct ON ct.code = ug.id_country
                     LEFT JOIN rating AS rt ON rt.item_id = it.id',
        ));

        $criteria->having = 'rating_count > 5';
        $criteria->group   = 'it.id';
        //$criteria->having  = 'rating > '.$this->item_rating;
        $criteria->order   = 'rating DESC';
        $criteria->index   = 'id';

        //$criteria->limit = $this->item_limit_navigation;
        $data = \Model\Item::model()->findAll( $criteria );

        return $data;
    }


    public function dataSightsCount()
    {
        $criteria    = (new \DBCriteria());
        $criteria->select = ' count(it.id) as count_sights ';

        $criteria->mergeWith(array(
            'join'=>'INNER JOIN item_has_locations AS ug ON ug.id = it.id
                         INNER JOIN countries AS ct ON ct.code = ug.id_country',
        ));

        $data = \Model\Item::model()->find( $criteria );
        return $data->count_sights;
    }

    public function getItemsImages( $data )
    {
        $criteria = new \DBCriteria();
        $criteria->select = 'gim.*, ihg.item_id, SUM(CASE WHEN ul.like = 1 THEN 1 ELSE 0 END) as votes';
        $criteria->addInCondition('ihg.item_id', array_keys($data));
        $criteria->group = 'gim.id';
        $criteria->order = 'votes DESC';
        $criteria->mergeWith(array(
            'join'=>'INNER JOIN item_has_gallery_images AS ihg ON ihg.gallery_images_id = gim.id
                     LEFT JOIN users_likes AS ul ON ul.id_image = gim.id'));
        $allImages = GalleryImages::model()->findAll($criteria);
        foreach ($allImages as $key=>$value) {
            $data_images[$value->item_id][$key]["user_id"] = $value->user_id;
            $data_images[$value->item_id][$key]["id"] = $value->id;
            $data_images[$value->item_id][$key]["src"] = $value->src;
        }
        return $data_images;
    }

    public function action_activation_success()
    {
        $content = $this->template->fetch('auth/activate_success.tpl');
        $this->response->body($content);
    }

}
