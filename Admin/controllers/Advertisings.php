<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 02.06.2017
 * Time: 12:04
 */

namespace Controller\Admin;

use Builder\System;
use Model\Advertising;
use Model\Cities;
use Model\Countries;
use Assets;
use Request;
use Exception;

class Advertisings extends System
{
    public function action_index()
    {

        Assets::js('datatable_jquery',base_UI.'js/plugins/datatables/jquery.dataTables.js');
        Assets::js('datatable',base_UI.'js/plugins/datatables/dataTables.bootstrap.js');

        Assets::js('icheck',base_UI.'js/plugins/iCheck/icheck.min.js');

        $this->addSelect2();

        $this->response->body( $this->template->fetch('admin/advertisings/Index.tpl') );
    }

    public function action_GetJson(){
        $access = new \Auth\Access(\Registry::getCurrentUser()->access_level);
        if($access->get(\Auth\Access::User_Is_Admin)) {
            $user_id = \Registry::getCurrentUser()->id;

            $data = \Model\Advertising::model()->with('country', 'city')->findAll();
            $countries = Countries::model()->findAll();


            $id = 1;
            $country_data = [];

            foreach($countries as $country) {
                $country_data[] = json_encode(['id' => $country['code'], 'text' => $country['name']]);
            }

            $aoColumnsData = [];

            foreach($data as $key=>$region)
            {

                $tmp = [
                    'id'          => $id,
                    'country'     => $region->country['code'],
                    'city'        => $region->city['name'],
                    'big'         => $region->bgScript,
                    'middle'      => $region->mdScript,
                    'small'       => $region->smScript,
                    'country_all' => $country_data,
                    'advId'       => $region->idAdv
                ];
                $aoColumnsData[] = $tmp;
                $id++;

            }

            $this->response->body(json_encode(['aaData' => $aoColumnsData]));
        }
        else
        {
            throw new \HTTP_Exception_403('Admin Only');
        }
    }

    public function action_getCountry(){
        $countries = Countries::model()->findAll();
        $country_data = [];
        $aoColumnsData = [];

        foreach($countries as $country) {

            $tmp = [
                'id' => $country['code'],
                'text' => $country['name']
            ];
            $aoColumnsData[] = $tmp;
        }

        $this->response->body(json_encode(['country' => $aoColumnsData]));

    }

    public function action_getCity(){
        $countryId = $this->request->post('country');
        $cities = Cities::model()->findAllByAttributes([], 'country = :country', [':country' => $countryId]);
        $country_data = [];
        $aoColumnsData = [];

        foreach($cities as $city) {

            $tmp = [
                'id' => $city['ID'],
                'text' => $city['name']
            ];

            $aoColumnsData[] = $tmp;
        }

        $this->response->body(json_encode(['city' => $aoColumnsData]));
    }

    public function action_saveAdv(){
        if( Request::current()->is_ajax() && !is_null($this->request->post()) ) {
            try {
                if (!is_null($this->request->post('advId'))) {
                    if($this->request->post('advId') == 'new'){

                        $adv = new Advertising();
                        $adv->idCountry = $this->request->post('country');
                        $adv->idCity = (!!$this->request->post('city')) ? $this->request->post('city') : null;
                        $adv->bgScript = (!!$this->request->post('big')) ? $this->request->post('big') : null;
                        $adv->mdScript = (!!$this->request->post('middle')) ? $this->request->post('middle') : null;
                        $adv->smScript = (!!$this->request->post('small')) ? $this->request->post('small') : null;
                        $result = $adv->save();
                        if(!$result){
                            throw new Exception(false);
                        }
                        $this->response->body(json_encode($result));
                    }
                    else if(is_int($advId = intval($this->request->post('advId')))) {

                        $adv = Advertising::model()->findByPk($advId);
                        $adv->idCountry = $this->request->post('country');
                        $adv->idCity = (!!$this->request->post('city')) ? $this->request->post('city') : null;
                        $adv->bgScript = (!!$this->request->post('big')) ? $this->request->post('big') : null;
                        $adv->mdScript = (!!$this->request->post('middle')) ? $this->request->post('middle') : null;
                        $adv->smScript = (!!$this->request->post('small')) ? $this->request->post('small') : null;
                        $result = $adv->update();
                        if(!$result){
                            throw new Exception(false);
                        }
                        $this->response->body(json_encode($result));
                    }
                    else {
                        throw new Exception(false);
                    }

                }
                else {
                    throw new Exception(false);
                }
            }
            catch (Exception $e) {
                $this->response->body(json_encode($e));
            }
        }
        else {
            return false;
        }

    }


    public function action_deleteAdv(){
        if( Request::current()->is_ajax() && !is_null($this->request->post()) ) {
            try {
                if (!is_null($this->request->post('advId')) && is_int($advId = intval($this->request->post('advId')))) {

                    $result = Advertising::model()->deleteByPk($advId);
                    if(!$result){
                        throw new Exception(false);
                    }
                    $this->response->body(json_encode($result));

                }
                else {
                    throw new Exception(false);
                }

            }
            catch (Exception $e) {
                $this->response->body(json_encode($e));
            }

        }
        else {
            return false;
        }

    }


}
