/**
 * Created by Java-PC on 02.06.2017.
 */
var selector = '#viewList';
jQuery(document).ready(function(){

    var DTObj = jQuery(selector).dataTable({
        "sPaginationType": "full_numbers",
        "bProcessing": true,
        "sAjaxSource": "GetJson/",
        "bDeferRender": true,
        "aoColumns":
            [
                { "mDataProp": "id", "sClass": "td-type"},
                // { "mDataProp": "region" },
                { "mData": null, "sClass": "td-region", "asSorting": {}, "sWidth": "10%"},
                { "mDataProp": "big", "sClass": "td-big" },
                { "mDataProp": "middle", "sClass": "td-md"  },
                { "mDataProp": "small", "sClass": "td-sm"  },
                { "mData": null, "sClass": "td-actions", "asSorting": {}, "sWidth": "10%" }
            ],
        "asStripeClasses": ['', ''],
        "fnServerParams": function(aoData)
        {

        },
        fnCreatedRow: function (nRow, aData, iDataIndex) {
            jQuery('.td-region', nRow).html('<div class="select-group">'+
                '<div>' +
                '<label for="country">Country</label><br/>' +
                '<input id="country_'+aData.id+'" name="country" class="country_'+aData.id+'" disabled value="'+ (aData.country ? aData.country : "") + '">' +
                '</div>' +
                '<div>' +
                '<label for="city">City</label><br/>' +
                '<input id="city_'+aData.id+'" name="city" class="city_'+aData.id+'" disabled value="'+ (aData.city ? aData.city : "") + '">' +
                '</div>' +
                '</div>');
            jQuery('.td-big', nRow).html('<textarea class="big_text_'+aData.id+'" disabled>' + aData.big +
                '</textarea>');
            jQuery('.td-md', nRow).html('<textarea class="md_text_'+aData.id+'" disabled>' + aData.middle +
                '</textarea>');
            jQuery('.td-sm', nRow).html('<textarea class="sm_text_'+aData.id+'" disabled>' + aData.small +
                '</textarea>');
            jQuery('.td-actions', nRow).append('<div class="btn-group">'+
                 '<button class="btn btn-default table-edit-item" data-id="' + aData.id + '"><i class="fa fa-pencil-square-o"></i></button>'+
                 '<button class="btn btn-default table-save-item" data-id="' + aData.id + '" data-advid="' + aData.advId + '"><i class="fa fa-floppy-o"></i></button>'+
                 '<button class="btn btn-default table-del-item" data-id="' + aData.id + '" data-advid="' + aData.advId + '"><i class="fa fa-trash-o"></i></button>'+
                 '</div>');


        }

    });

    jQuery(selector).on('click','.table-edit-item',function(e) {
        var id = $(this).attr('data-id');
        $('.country_'+id).addClass('js-single-country_'+id).attr('disabled', false);
        $('.city_'+id).addClass('js-single-city_'+id).attr('disabled', false);
        $('.big_text_'+id+', .md_text_'+id+', .sm_text_'+id).attr('disabled', false);

        // alert(id);
        jQuery.ajax({
            async: true,
            type: "POST",
            url: 'getCountry',
            dataType: 'json',
            success: function(data) {
                console.log(data);
                $('.js-single-country_'+id).select2({
                    data: data.country
                });
            }
        });

        $('.js-single-city_'+id).on('click', function() {
            city();
        });
        $('.js-single-country_'+id).on('change', function(){
            city();
        });

        function city(){
            var countryId = $('#country_'+id).val();

            jQuery.ajax({
                type: "POST",
                data: { country: countryId},
                url: 'getCity',
                dataType: 'json',
                success: function(data) {
                    $('.js-single-city_'+id).select2({
                        data: data.city
                    });
                }
            });
        }
    });

    // for add row
    var counter = 1;

    $('.new_adv').on('click', function(){
      $('#viewList').dataTable().fnAddData({
            'id': 'new'+counter,
            'region' : '',
            'big' : '',
            'middle' : '',
            'small' : ''
        });

        counter++;
    });


    $(selector).on('click','.table-save-item',function(e) {
        var id = $(this).attr('data-id');
        var advId = $(this).attr('data-advid');
        if(advId == 'undefined' || advId == 'NaN'){
            advId = 'new';
        }
        else {
            advId = +advId;
        }
        var country = $('#country_'+id).val();
        var city = $('#city_'+id).val();
        var big = $('.big_text_'+id).val();
        var middle = $('.md_text_'+id).val();
        var small = $('.sm_text_'+id).val();

        $.ajax({
            async: true,
            type: "POST",
            data: {advId: advId, country: country, city: city, big: big, middle: middle, small: small},
            url: 'saveAdv',
            success: function (e) {
                bootbox.alert('Changes accepted');
                $('.country_'+id).removeClass('js-single-country_'+id).attr('disabled', true);
                $('.city_'+id).removeClass('js-single-city_'+id).attr('disabled', true);
                $('.big_text_'+id+', .md_text_'+id+', .sm_text_'+id).attr('disabled', true);
            },
            error: function (e) {
                bootbox.alert('Sorry! Something wrong. Please try again later.');
            }
        })
    });


    $(selector).on('click','.table-del-item',function(event) {
        var id = $(this).attr('data-id');
        var advId = $(this).attr('data-advid');
        if(isNaN(advId)) {
            bootbox.alert('Sorry! This item is impossible to delete');
            location.reload();
        }
        $.ajax({
            async: true,
            type: "POST",
            data: {advId: advId},
            url: 'deleteAdv',
            success: function (e) {
                bootbox.alert('Item has been deleted');
                var nTr = event.target.parentNode;
                while ( nTr ){
                    if ( nTr.nodeName == "TR" ) break;
                    nTr = nTr.parentNode;
                }
                var iPos= $('#viewList').dataTable().fnGetPosition(nTr);
                $('#viewList').dataTable().fnDeleteRow(iPos);

            },
            error: function (e) {
                bootbox.alert('Sorry! Something wrong. Please try again later.');
            }
        })

    });


});