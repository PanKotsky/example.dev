{extends file="../../layout/System/frame.tpl"}
{block name=content}

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">

                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <br>
                        <button class="new_adv">Add new</button>
                        <table id="viewList" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>Region</th>
                                <th>Script (big size)</th>
                                <th>Script (middle size)</th>
                                <th>Script (small size)</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>id</th>
                                <th>Region</th>
                                <th>Script (big size)</th>
                                <th>Script (middle size)</th>
                                <th>Script (small size)</th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box-header -->
                <div class="box-body table-responsive">
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        </div>
    </section>

    <!-- /.content -->
{/block}
{block name="jscode"}
    <script type="text/javascript">
    </script>
{/block}